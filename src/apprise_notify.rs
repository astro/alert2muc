use askama::Template;
use serde::Deserialize;

#[derive(Debug, Clone, Deserialize, Template)]
#[template(path="apprise_notify.txt", escape="none")]
pub struct Notification {
    pub title: String,
    pub message: String,
    pub r#type: String,
}
