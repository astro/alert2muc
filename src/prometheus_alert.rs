use askama::Template;
use serde::Deserialize;

#[derive(Debug, Clone, Deserialize, Template)]
#[template(path="prometheus_alert.txt", escape="none")]
pub struct Payload {
    pub alerts: Vec<Alert>,
}

#[derive(Debug, Clone, Deserialize)]
pub struct Alert {
    pub status: String,
    pub labels: AlertLabels,
    pub annotations: AlertAnnotations,
    #[serde(rename = "generatorURL")]
    pub generator_url: String,
}

#[derive(Debug, Clone, Deserialize)]
pub struct AlertLabels {
    pub alertname: Option<String>,
    pub host: Option<String>,
    pub instance: Option<String>,
    pub exported_instance: Option<String>,
}

#[derive(Debug, Clone, Deserialize)]
pub struct AlertAnnotations {
    pub message: Option<String>,
    pub description: Option<String>,
}
