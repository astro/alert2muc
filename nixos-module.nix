{ self }:
{ config, lib, pkgs, ... }: {
  options.services.alert2muc = with lib; {
    enable = mkEnableOption "Enable Prometheus XMPP MUC bot";
    configFile = mkOption {
      type = types.str;
    };
    user = mkOption {
      type = types.str;
      default = "alert2muc";
    };
    group = mkOption {
      type = types.str;
      default = "alert2muc";
    };
  };

  config =
    let
      cfg = config.services.alert2muc;
    in
      lib.mkIf cfg.enable {
        users = {
          users.${cfg.user} = {
            isSystemUser = true;
            group = cfg.group;
          };
          groups.${cfg.group} = {};
        };

        systemd.services.alert2muc = {
          wantedBy = [ "multi-user.target" ];
          after = [ "network-online.target" ];
          wants = [ "network-online.target" ];
          serviceConfig = {
            Type = "notify";
            ExecStart = "${lib.getExe self.packages.${pkgs.stdenv.system}.alert2muc} ${lib.escapeShellArg cfg.configFile}";
            User = cfg.user;
            Group = cfg.group;
            ProtectSystem = "full";
            Restart = "always";
            RestartSec = "10s";
            WatchdogSec = "3600s";
          };
        };
      };
}
